package classes;

import java.io.IOException;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.XMLOutputter;

/**
 * A class to submit O&M as well as SensorML documents to an SOS instance.
 * 
 * @author Dezhi Fu
 */

public class SOSClient {
  private String GetCapabilitiesResponse;
  private String getObservationResponse;
  // save the GetObservation response to Tomcat webapps directory to feed the JS
  // client
  private static final String responseStandardOutputPath = "C:\\Program Files\\Apache Software Foundation\\Tomcat 9.0\\webapps\\MySOSClient\\GetObservationResponse.xml";
  private static final String sosInstanceAddress = "http://localhost:8080/52n-sos-webapp/service";

  /**
   * Constructor which inserts the telemetry system SensorML description if the
   * GetCapabilities response does not indicate any telemetry system
   * registration.
   */
  public SOSClient() {
    if (!observationProcedureRegistered(GetCapabilities()))
      InsertSensor();
  }

  /**
   * This method applies InsertObservation operation of SOS standard using a
   * static dynamically modified O&M document and HTTP POST.
   * 
   * @param observationsAndMeasurementsDocument
   *          a JDOM-based representation of the O&M document.
   * @return the instance itself
   */

  public SOSClient InsertObservation(Document observationsAndMeasurementsDocument) {
    postDocument(observationsAndMeasurementsDocument);
    return this;
  }

  /**
   * This method applies GetCapabilities operation of SOS standard using a
   * static dynamically modified O&M document and HTTP POST. It further saves
   * the response to an instance variable and returns it.
   */

  public String GetCapabilities() {
    XMLParser parser = new XMLParser();
    parser.readXML("/resources/GetCapabilities.xml");
    setGetCapabilitiesResponse(postDocument(parser.getXmlDocument()));
    return getGetCapabilitiesResponse();
  }

  /**
   * This method applies GetObservation operation of SOS standard using a static
   * dynamically modified O&M document and HTTP POST. It further saves the
   * response to an instance variable.
   * 
   * @return the instance itself
   */

  public SOSClient GetObservation() {
    XMLParser parser = new XMLParser();
    parser.readXML("/resources/GetObservation.xml");
    setGetObservationResponse(postDocument(parser.getXmlDocument()));
    return this;
  }

  /**
   * This method saves the SOS response and writes it to a XML document.
   * 
   * @return the instance itself
   */

  public SOSClient saveResponse(String serverResponse, String outputPath) {
    XMLParser parser = new XMLParser();
    parser.writeXML(serverResponse, outputPath);
    return this;
  }

  /**
   * This method applies InsertSensor operation of SOS standard using a static
   * SensorML document and HTTP POST.
   * 
   * @return the instance itself
   */

  public SOSClient InsertSensor() {
    XMLParser parser = new XMLParser();
    parser.readXML("/resources/InsertSensor.xml");
    postDocument(parser.getXmlDocument());
    return this;
  }

  /**
   * This method executes HTTP POST method using Apache HttpClient API. It
   * accepts a XML document coming from JDOM based representation and submits
   * the data to a SOS using hard coded address.
   * 
   * @param doc
   *          JDOM based representation of XML document which shall be submitted
   *          to server/SOS.
   * @return the server response
   */

  public String postDocument(Document doc) {
    // Get the string representation of Document instance
    String xml = new XMLOutputter().outputString(doc);
    // set HTTP request body content and type of the content
    StringEntity entity = new StringEntity(xml, ContentType.create("application/xml"));
    // build client
    CloseableHttpClient httpClient = HttpClientBuilder.create().build();
    // specify server address and generate POST request
    HttpPost post = new HttpPost(sosInstanceAddress);
    // attach message to request
    post.setEntity(entity);
    // initialize response and result of the server
    CloseableHttpResponse response = null;
    String result = null;
    // execute HTTP POST
    try {
      // To get server response print out getStatusLine()-method of response,
      // but here we just execute the HTTP-POST
      response = httpClient.execute(post);
      result = EntityUtils.toString(response.getEntity());
      System.out.println(result);
      // final steps
      if (entity != null)
        EntityUtils.consume(entity);
      post.reset();
    } catch (ClientProtocolException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      try {
        response.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    return result;
  }

  /**
   * This method checks if the telemetry system is already registered and known
   * by the SOS using XPath.
   * 
   * @param response
   *          of the server which has to be analyzed.
   * @return a boolean which specifies whether the procedure is registered or
   *         not.
   */

  public boolean observationProcedureRegistered(String response) {
    XMLParser parser = new XMLParser();
    List<Element> procedures;
    try {
      procedures = parser.getElementsByXpath(".//swes:procedure", parser.getSwes(), parser.buildDOM(response));
    } catch (NullPointerException e) {
      return false;
    }
    for (Element procedure : procedures) {
      if (procedure.getText().compareTo("http://www.52north.org/test/procedure/telemetrySystem") == 0) {
        System.out.println(procedure.getText() + " is already registered at SOS.");
        return true;
      }
    }
    return false;
  }

  // Getters and Setters
  public String getGetObservationResponse() {
    return getObservationResponse;
  }

  public void setGetObservationResponse(String getObservationResponse) {
    this.getObservationResponse = getObservationResponse;
  }

  public static String getResponseStandardOutputPath() {
    return responseStandardOutputPath;
  }

  public String getGetCapabilitiesResponse() {
    return GetCapabilitiesResponse;
  }

  public void setGetCapabilitiesResponse(String getCapabilitiesResponse) {
    GetCapabilitiesResponse = getCapabilitiesResponse;
  }
}
