package classes;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * A class to establish socket-based connections using java.net package.
 * 
 * @author Dezhi Fu
 */
public class Server {
  private ServerSocket serverSocket;
  private Hashtable<Socket, DataOutputStream> outputStreams = new Hashtable<Socket, DataOutputStream>();

  /**
   * Constructor which calls the listen method on a specified port.
   * 
   * @param port
   *          the port on which the server can be reached
   * @author Dezhi Fu
   */

  public Server(int port) {

    // We listen to the port for incoming connection requests
    try {
      listen(port);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Using ServerSocket instance this method wait for incoming connection
   * attempts. It returns a Socket instance which can be used to send data in
   * and read data out.
   * 
   * @param port
   *          the port on which the Server instance listens to incoming
   *          connection attempts
   * @author Dezhi Fu
   */
  private void listen(int port) throws IOException {
    serverSocket = new ServerSocket(port);
    System.out.println("Listen to port: " + serverSocket.getLocalPort());
    // Listen to the port and accept incoming connection requests
    while (true) {
      // return a Socket instance, if connection was established
      Socket s = serverSocket.accept();
      System.out.println("Connection established to socket " + s);
      // Generate data stream to send data to client (possible feature)
      DataOutputStream dout = new DataOutputStream(s.getOutputStream());
      outputStreams.put(s, dout);
      // Pass the socket to a thread which deals with data saving to database
      new ServerThread(this, s);
    }
  }

  /**
   * 
   * Sends a message to all connected clients.
   * 
   * @param message
   *          The message as A String which is to be sent to the clients.
   */

  void sendToAll(String message) {
    // synchronuous message sending
    synchronized (outputStreams) {
      // For each client
      for (Enumeration<DataOutputStream> e = getOutputStreams(); e.hasMoreElements();) {
        // select communication channel to client
        DataOutputStream dout = (DataOutputStream) e.nextElement();
        // send message
        try {
          dout.writeUTF(message);
        } catch (IOException ie) {
          ie.printStackTrace();
        }
      }
    }
  }

  /**
   * 
   * Removes client connection.
   * 
   * @param s
   *          The TCP socket of the client
   */
  void removeConnection(Socket s) {
    synchronized (outputStreams) {
      System.out.println("Remove connection from socket " + s);
      // remove socket
      outputStreams.remove(s);
      // close connection
      try {
        s.close();
      } catch (IOException ie) {
        System.out.println("Error while closing socket " + s);
        ie.printStackTrace();
      }
    }
  }

  // Getter
  Enumeration<DataOutputStream> getOutputStreams() {
    return outputStreams.elements();
  }

}
