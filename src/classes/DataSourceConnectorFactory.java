package classes;

import java.util.Properties;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

/**
 * * A class to connect to databases using DataSource instances. Using the
 * provided methods one can connect to different databases.
 * 
 * @author Dezhi Fu
 */
public class DataSourceConnectorFactory {
  private static final String POSTGRES_DATA_SOURCE_CLASS_NAME = "org.postgresql.ds.PGPoolingDataSource";
  private static final String USER = "postgres";
  private static final String PASSWORD = "p0stgr3s";

  /** Don't let anyone instantiate this class */
  private DataSourceConnectorFactory() {
  }

  /**
   * This method creates a connection to the 'telemetrySystemDatabase' which was
   * preconfigured using pgAdmin. Here we save the telemetry data of our
   * datalogger.
   */
  public static HikariDataSource getPostgresqlDataSource() {
    final String DB_NAME = "telemetrySystemDatabase";
    Properties props = new Properties();
    props.setProperty("dataSourceClassName", POSTGRES_DATA_SOURCE_CLASS_NAME);
    props.setProperty("dataSource.user", USER);
    props.setProperty("dataSource.password", PASSWORD);
    props.setProperty("dataSource.databaseName", DB_NAME);
    props.setProperty("dataSource.portNumber", "1890");
    return new HikariDataSource(new HikariConfig(props));
  }

  /**
   * This method creates a connection to the 'testDatabase' which was
   * preconfigured using pgAdmin. Here we save test data. The test data is hard
   * coded as class variables of the class 'DataSourceConnector' and is used in
   * JUnit tests to check the functionality of the 'DataSourceConnector' class.
   */
  public static HikariDataSource getTestPostgresqlDataSource() {
    final String DB_NAME = "testDatabase";
    Properties props = new Properties();
    props.setProperty("dataSourceClassName", POSTGRES_DATA_SOURCE_CLASS_NAME);
    props.setProperty("dataSource.user", USER);
    props.setProperty("dataSource.password", PASSWORD);
    props.setProperty("dataSource.databaseName", DB_NAME);
    props.setProperty("dataSource.portNumber", "1890");
    return new HikariDataSource(new HikariConfig(props));
  }
}
