package classes;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;

import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.filter.Filters;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;

/**
 * This class is using JDOM in order to pass the instance variables values of a
 * Vehicle instance to O&M-Documents. It further deals with XML processing.
 * 
 * @author Dezhi Fu
 */

public class XMLParser {

  private Document xmlDocument;
  private Element root;
  private Element offering;
  private Element observation;
  private String xmlPath;
  private Vehicle vehicleData;
  private Namespace sos;
  private Namespace gml;
  private Namespace om;
  private Namespace xlink;
  private Namespace xs;
  private Namespace xsi;
  private Namespace swes;
  private String identifier;

  /**
   * Constructor which initializes Namespaces needed for O&M, SensorML
   * processing.
   */

  public XMLParser() {
    sos = Namespace.getNamespace("sos", "http://www.opengis.net/sos/2.0");
    gml = Namespace.getNamespace("gml", "http://www.opengis.net/gml/3.2");
    om = Namespace.getNamespace("om", "http://www.opengis.net/om/2.0");
    xlink = Namespace.getNamespace("xlink", "http://www.w3.org/1999/xlink");
    xs = Namespace.getNamespace("xs", "http://www.w3.org/2001/XMLSchema");
    xsi = Namespace.getNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
    swes = Namespace.getNamespace("swes", "http://www.opengis.net/swes/2.0");
  }

  /**
   * This method accepts the path to a xml file and generates from the xml
   * document a JDOM-based tree. The document is saved as an instance variable
   * of the parser instance, you can get it using the getter method
   * getXMLDocument()
   * 
   * @param xmlPath
   *          the path to the to be processed XML file
   * @return the instance itself
   */

  public XMLParser readXML(String xmlPath) {
    // set the path and generate file instance, which represents the XML
    // document
    setXMLPath(xmlPath);
    File xmlFile = new File(getClass().getResource(getXMLPath()).getPath());
    // Prepare construction of JDOM tree
    SAXBuilder builder = new SAXBuilder();

    try {
      // returns XML Document as a JDOM tree
      xmlDocument = (Document) builder.build(xmlFile);
      // set the root element and the tree
      setRoot(xmlDocument.getRootElement());
      setXmlDocument(xmlDocument);
    } catch (IOException io) {
      System.out.println(io.getMessage());
    } catch (JDOMException jdomex) {
      System.out.println(jdomex.getMessage());
    }
    return this;
  }

  /**
   * This method accepts String representation of a XML document and returns the
   * JDOM-based representation of that document.
   * 
   * @param xmlDocumentString
   *          the String representation of a XML document.
   * @return the JDOM based representation of that document
   */

  public Document buildDOM(String xmlDocumentString) {
    SAXBuilder builder = new SAXBuilder();
    try {
      return (Document) builder.build(new StringReader(xmlDocumentString));
    } catch (JDOMException | IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  /**
   * This method accepts String representation of a XML document and returns the
   * file representation, i.e. the actual document in XML format.
   * 
   * @param xmlString
   *          the String representation of a XML document.
   * @param outputPath
   *          the output path to which the file is written
   */

  public void writeXML(String xmlString, String outputPath) {

    XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());

    try {
      xmlOutputter.output(buildDOM(xmlString), new FileOutputStream(outputPath));
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * After reading a O&M document and setting the root element of that document,
   * this method accepts Vehicle objects and parse the values of the instance
   * variables to O&M documents.
   * 
   * @param vehicle
   *          the vehicle instance which contains process data to parse to O&M
   *          files.
   * @return the O&M document which contains process data
   */

  public Document marshal(Vehicle vehicle) {
    setVehicleData(vehicle);
    // identifier for the observations
    setIdentifier(getVehicleData().getTimestamp().replaceAll("[^0-9]", ""));
    // get offerings and observations in the O&M document
    List<Element> observations = root.getChildren();

    // skip first node (which is offering) for the other nodes, which are the
    // observations, insert observation properties
    for (Element observation : observations) {
      if (observation == observations.get(0))
        continue;
      insertObservationID(observation);
      insertObservationTime(observation);
      insertObservationParameters(observation);
      insertObservationResult(observation);
    }
    return xmlDocument;
  }

  /**
   * This method accepts O&M elements and modifies the observationID using the
   * instance variable identifier.
   * 
   * @param observation
   *          the O&M observation element which we want to modify
   */

  private void insertObservationID(Element observation) {
    // get the ID of the observation and attach a unique identifier to it
    Element observationID = observation.getChild("OM_Observation", om).getChild("identifier", gml);
    observationID.setText(observationID.getText() + getIdentifier());
  }

  /**
   * This method accepts O&M elements and modifies the phenomenon time.
   * 
   * @param observation
   *          the O&M phenomenon time element which we want to modify
   */

  private void insertObservationTime(Element observation) {
    // if there is a concrete time given, i.e. if there is a corresponding
    // child, modify it accordingly
    if (!observation.getChild("OM_Observation", om).getChild("phenomenonTime", om).getChildren().isEmpty()) {
      Element phenomenonTime = observation.getChild("OM_Observation", om).getChild("phenomenonTime", om);
      // set the time of the element according to timestamp of vehicle instance
      phenomenonTime.getChild("TimeInstant", gml).getChild("timePosition", gml)
          .setText(getVehicleData().getTimestamp());
    }
  }

  /**
   * This method accepts O&M elements and modifies all parameters. In this
   * implementation if you add new parameters to the InsertObservation document
   * you have to extend the switch-case-construct
   * 
   * @param observation
   *          the O&M element which we want to modify
   */

  private void insertObservationParameters(Element observation) {
    // pick all parameters including position and quality parameters
    List<Element> observationParameters = observation.getChild("OM_Observation", om).getChildren("parameter", om);
    for (Element observationParameter : observationParameters) {
      Attribute parameterName = observationParameter.getChild("NamedValue", om).getChild("name", om)
          .getAttribute("href", xlink);
      switch (parameterName.getValue()) {
      case "NumberOfSatellitesInView":
        observationParameter.getChild("NamedValue", om).getChild("value", om)
            .setText(String.valueOf(vehicleData.getNumberOfSatellitesTracked()));
        break;
      case "http://www.opengis.net/def/param-name/OGC-OM/2.0/samplingGeometry":
        observationParameter.getChild("NamedValue", om).getChild("value", om).getChild("Point", gml)
            .getChild("pos", gml).setText(vehicleData.getLatitude() + " " + vehicleData.getLongitude());
        break;
      }
    }
  }

  /**
   * This method accepts O&M elements and modifies the observation result.
   * 
   * @param observation
   *          the O&M result element which we want to modify
   */
  private void insertObservationResult(Element observation) {
    Attribute observedPropertyXLink = observation.getChild("OM_Observation", om).getChild("observedProperty", om)
        .getAttribute("href", xlink);
    Element observationResult = observation.getChild("OM_Observation", om).getChild("result", om);
    // set the observation results
    if (observedPropertyXLink.getValue().contains("engineRPM")) {
      observationResult.setText(String.valueOf(vehicleData.getEngineRPM()));
    } else if (observedPropertyXLink.getValue().contains("fuelConsumption")) {
      observationResult.setText(String.valueOf(vehicleData.getFuelConsumption()));
    }
  }

  /**
   * This method uses XPath to find a element in a JDOM-based representation of
   * a XML document.
   * 
   * @param xPath
   *          XPath expression to search for an element
   * @param ns
   *          Namespace which is associated with the elements
   * @param doc
   *          XML document which contains the desired elements
   * @return A list containing the found elements
   */

  public List<Element> getElementsByXpath(String xPath, Namespace ns, Document doc) {
    // use the default implementation
    XPathFactory xFactory = XPathFactory.instance();
    // select all links
    XPathExpression<Element> expr = xFactory.compile(xPath, Filters.element(), null, ns);
    List<Element> searchedElements = expr.evaluate(doc);
    return searchedElements;
  }

  // Getter, Setter
  /**
   * This method returns the Document representation of the XML document of the
   * current parser instance.
   */

  public Document getXmlDocument() {
    return xmlDocument;
  }

  public void setXmlDocument(Document xmlDocument) {
    this.xmlDocument = xmlDocument;
  }

  public Element getOffering() {
    return offering;
  }

  public void setOffering(Element offering) {
    this.offering = offering;
  }

  public Element getObservation() {
    return observation;
  }

  public void setObservation(Element observation) {
    this.observation = observation;
  }

  public String getXMLPath() {
    return xmlPath;
  }

  public void setXMLPath(String xmlPath) {
    this.xmlPath = xmlPath;
  }

  public Element getRoot() {
    return root;
  }

  public void setRoot(Element root) {
    this.root = root;
  }

  public Vehicle getVehicleData() {
    return vehicleData;
  }

  public void setVehicleData(Vehicle vehicle) {
    this.vehicleData = vehicle;
  }

  public Namespace getSwes() {
    return swes;
  }

  public void setSwes(Namespace swes) {
    this.swes = swes;
  }

  public Namespace getSos() {
    return sos;
  }

  public void setSos(Namespace sos) {
    this.sos = sos;
  }

  public String getIdentifier() {
    return identifier;
  }

  public void setIdentifier(String identifier) {
    this.identifier = identifier;
  }
}
