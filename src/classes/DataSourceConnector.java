package classes;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import com.zaxxer.hikari.HikariDataSource;

/**
 * Class for manipulating and querying telemetry data using plain SQL
 * statements. The statements are hard coded in the source code to minimize
 * security risk potential and to ensure comprehensibility. If further
 * statements are needed there is the possibility to outsource the SQL
 * statements in external resources to keep code clean.
 * 
 * @author Dezhi Fu
 */
public class DataSourceConnector {
  private static HikariDataSource dataSource;
  // create table sql statements
  private static final String sqlCreateNMEABasicTable = "CREATE TABLE IF NOT EXISTS nmeadata("
      + "id BIGSERIAL NOT NULL PRIMARY KEY," + "start_counter INTEGER NOT NULL, "
      + "local_time DOUBLE PRECISION NOT NULL," + "longitude DOUBLE PRECISION NOT NULL,"
      + "latitude DOUBLE PRECISION NOT NULL," + "altitude DOUBLE PRECISION NOT NULL," + "utc TEXT NOT NULL,"
      + "time_to_first_fix INTEGER NOT NULL," + "number_of_satellites_in_view SMALLINT NOT NULL,"
      + "speed_over_ground DOUBLE PRECISION NOT NULL," + "course_over_ground DOUBLE PRECISION NOT NULL);";
  private static final String sqlCreateRPMTable = "CREATE TABLE IF NOT EXISTS rpmdata("
      + "id BIGSERIAL NOT NULL PRIMARY KEY," + "start_counter INTEGER NOT NULL, "
      + "local_time_identity DOUBLE PRECISION NOT NULL ," + "rpm_value SMALLINT NOT NULL);";
  private static final String sqlCreateFuelConsumptionTable = "CREATE TABLE IF NOT EXISTS fuelconsumptiondata("
      + "id BIGSERIAL NOT NULL PRIMARY KEY," + "start_counter INTEGER NOT NULL, "
      + "local_time_identity DOUBLE PRECISION NOT NULL ," + "fuel_consumption_value DOUBLE PRECISION NOT NULL);";
  // drop table sql statements
  private static final String sqlDropNMEABasicTable = "DROP TABLE IF EXISTS nmeadata CASCADE;";
  private static final String sqlDropRPMTable = "DROP TABLE IF EXISTS rpmdata CASCADE;";
  private static final String sqlDropFuelConsumptionTable = "DROP TABLE IF EXISTS fuelconsumptiondata CASCADE;";
  // create view sql statement
  private static final String sqlCreateView = "CREATE OR REPLACE VIEW georeferencedTelemetryData AS "
      + "SELECT nmeadata.id," + "nmeadata.longitude," + "nmeadata.latitude," + "nmeadata.altitude," + "nmeadata.utc,"
      + "nmeadata.time_to_first_fix," + "nmeadata.number_of_satellites_in_view," + "nmeadata.speed_over_ground,"
      + "nmeadata.course_over_ground," + "rpmdata.rpm_value," + "fuelconsumptiondata.fuel_consumption_value FROM "
      + "nmeadata, rpmdata, fuelconsumptiondata," + "(SELECT DISTINCT ON (nmeadata.id) nmeadata.id as nmeadata_id,"
      + "rpmdata.id as rpmdata_id," + "fuelconsumptiondata.id as fuelconsumption_id,"
      + "nmeadata.start_counter as nmeadata_start_counter," + "rpmdata.start_counter as rpmdata_start_counter,"
      + "fuelconsumptiondata.start_counter as fuelconsumptiondata_start_counter,"
      + "abs(nmeadata.local_time-rpmdata.local_time_identity) as abs_time_difference_rpmdata,"
      + "abs(nmeadata.local_time-fuelconsumptiondata.local_time_identity) as abs_time_difference_fuelconsumptiondata "
      + "FROM public.nmeadata, public.rpmdata, public.fuelconsumptiondata "
      + "WHERE nmeadata.start_counter = rpmdata.start_counter "
      + "AND rpmdata .start_counter = fuelconsumptiondata.start_counter "
      + "ORDER BY nmeadata_id, abs_time_difference_rpmdata, abs_time_difference_fuelconsumptiondata ASC)inner_table "
      + "WHERE (nmeadata.id = nmeadata_id AND rpmdata.id = inner_table.rpmdata_id "
      + "AND fuelconsumptiondata.id = inner_table.fuelconsumption_id) " + "AND (abs_time_difference_rpmdata < 0.5 "
      + "AND abs_time_difference_fuelconsumptiondata < 0.5)";
  // select paginated view sql statement
  private static String sqlSelectViewSublist = "SELECT * FROM georeferencedTelemetryData ORDER BY georeferencedTelemetryData.id LIMIT %d OFFSET %d";
  // offset view rows
  private static int offsetRows = 0;

  /**
   * Assign a HikariDataSource to static DataSource.
   * 
   * @param ds
   *          a HikariConfig instance
   */
  public DataSourceConnector(HikariDataSource ds) {
    dataSource = ds;
  }

  /**
   * Call the close()-method of the DataSource-instance to shutdown the
   * DataSource and its associated pool.
   */
  public void closeDataSource() {
    dataSource.close();
  }

  /**
   * Drop all tables and linked dependencies (e. g. views).
   * 
   */
  public void dropTables() {
    dropTable(sqlDropNMEABasicTable);
    dropTable(sqlDropRPMTable);
    dropTable(sqlDropFuelConsumptionTable);
  }

  /**
   * Drop single database table using the static DROP TABLE SQL statements of
   * this class.
   * 
   * @param sqlStatement
   *          One of the DROP SQL statements which are hard coded in the class
   *          definition.
   */
  private void dropTable(final String sqlStatement) {
    try (Connection conn = dataSource.getConnection()) {
      // Drop existing table
      try (Statement stmt = conn.createStatement()) {
        stmt.executeUpdate(sqlStatement);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  /**
   * Create view of the georeferenced telemetry data using the static CREATE
   * VIEW SQL statements of this class.
   */
  public void createView() {
    try (Connection conn = dataSource.getConnection()) {
      // Drop existing table
      try (Statement stmt = conn.createStatement()) {
        stmt.executeUpdate(sqlCreateView);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  /**
   * Get a List of vehicle instances using the
   * selectGeoreferencedTelemetryDataView()-method.
   */
  public List<Vehicle> getVehicleData() {
    return selectGeoreferencedTelemetryDataView();
  }

  /**
   * Return a list of vehicle instances. First execute a SELECT SQL Statement to
   * get the VIEW of georeferenced telemetry data. Use pagination technique to
   * ensure selecting a subset of the total result set. The number of retrieved
   * rows is set by the value of the 'limitNumberOfRows' variable.
   * 
   */
  private List<Vehicle> selectGeoreferencedTelemetryDataView() {
    int limitNumberOfRows = 10;
    String sql = String.format(sqlSelectViewSublist, limitNumberOfRows, offsetRows);
    List<Vehicle> vehicleList = new ArrayList<Vehicle>();
    try (Connection conn = dataSource.getConnection()) {
      // enable scrolling of rows, which are sensitive to changed data
      Statement stmt = createStatement(conn, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
      // give the database 'hints' how many rows should be returned
      stmt.setFetchSize(limitNumberOfRows);
      stmt.setMaxRows(limitNumberOfRows);
      try (ResultSet rs = stmt.executeQuery(sql)) {
        while (rs.next()) {
          double longitude = rs.getDouble("longitude");
          double latitude = rs.getDouble("latitude");
          // double altitude = rs.getDouble("altitude");
          String timestamp = rs.getString("utc");
          // int ttff = rs.getInt("time_to_first_fix");
          short numberOfSatellitesInView = rs.getShort("number_of_satellites_in_view");
          // double speedOverGround = rs.getDouble("speed_over_ground");
          // double courseOverGround = rs.getDouble("course_over_ground");
          short rpmValue = rs.getShort("rpm_value");
          double fuelConsumptionValue = rs.getDouble("fuel_consumption_value");
          vehicleList.add(
              new Vehicle(longitude, latitude, timestamp, numberOfSatellitesInView, rpmValue, fuelConsumptionValue));
        }
      } catch (SQLException e) {
        e.printStackTrace();
      }
    } catch (SQLException e1) {
      e1.printStackTrace();
    }
    // adjust offset parameter to ensure reading rows from new offset on
    offsetRows = offsetRows + vehicleList.size();
    return vehicleList;
  }

  /**
   * Create all tables used in the telemetry system workchain.
   * 
   */
  public void createTables() {
    createTable(sqlCreateNMEABasicTable);
    createTable(sqlCreateRPMTable);
    createTable(sqlCreateFuelConsumptionTable);
  }

  /**
   * Create single database table using the static CREATE TABLE SQL statements
   * of this class.
   * 
   * @param sqlStatement
   *          One of the CREATE SQL statements which are hard coded in the class
   *          definition.
   */
  private void createTable(String createTableStatement) {
    try (Connection conn = dataSource.getConnection()) {
      // Create tables
      try (PreparedStatement pstmt = conn.prepareStatement(createTableStatement)) {
        try {
          pstmt.executeUpdate();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      } catch (SQLException e1) {
        e1.printStackTrace();
      }
    } catch (SQLException e2) {
      e2.printStackTrace();
    }
  }

  /**
   * Insert values into database using a String of incoming data
   * 
   * @param linesOfData
   *          A concatenated String of semicolon-separated sentences containing
   *          telemetry data. The values in one sentence are separated by
   *          commata.
   * @return A int array containing the number of inserted rows for each of the
   *         three data groups, i. e. NMEA-sentences, RPM-sentences, and Fuel
   *         Consumption-sentences.
   */
  public int[] insertValues(String linesOfData) {
    // SQL parameterized Insert-statements
    String sqlForInsertNMEA = ("INSERT INTO nmeadata VALUES (Default, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
    String sqlForInsertRPM = ("INSERT INTO rpmdata VALUES (Default, ?, ?, ?)");
    String sqlForInsertFuelConsumption = ("INSERT INTO fuelconsumptiondata VALUES (Default, ?, ?, ?)");
    // strip whitespace and parse data using delimiter semicolon
    String[] separatedSentences = linesOfData.trim().split(";");
    System.out.println();
    System.out.println("Number of incoming telemetry system data sentences: " + separatedSentences.length);
    // prepare PreparedStatement-instances
    PreparedStatement psNMEA = null;
    PreparedStatement psRPM = null;
    PreparedStatement psFuelConsumption = null;
    // hold batch constant to limit the number of inserts
    final int batchSize = 1000;
    int batchCount = 0;
    // count the number of inserts
    int insertNMEACounter = 0;
    int insertRPMCounter = 0;
    int insertFuelConsumptionCounter = 0;
    // get database connection
    try (Connection conn = dataSource.getConnection()) {
      // create SQL statements
      psNMEA = createPreparedStatement(conn, sqlForInsertNMEA);
      psRPM = createPreparedStatement(conn, sqlForInsertRPM);
      psFuelConsumption = createPreparedStatement(conn, sqlForInsertFuelConsumption);
      // for each semicolon-separated sentence we're going to parse them
      // appropriately
      for (int i = 0; i < separatedSentences.length; i++) {
        System.out.println("--------------------------------");
        System.out.println("Parsing sentence " + (i + 1) + " from " + separatedSentences.length + ":");
        System.out.println();
        System.out.println(separatedSentences[i]);
        // separate values in one sentence, '_'-delimiter is used to split the
        // local time identity which consists of a start counter and the local
        // timestamp of the measurement data logger (e. g. 6_1469695189.109694)
        String[] separatedSentence = separatedSentences[i].split(",|_");
        int startCounter = Integer.parseInt(separatedSentence[0]);
        double localTimestamp = Double.parseDouble(separatedSentence[1]);
        // if we have a NMEA sentence (contains 11 values), parse it accordingly
        // and add to our batch
        
        if (separatedSentence.length == 11) {
          double longitude = Double.parseDouble(separatedSentence[3]);
          double latitude = Double.parseDouble(separatedSentence[4]);
          double altitude = Double.parseDouble(separatedSentence[5]);
          String timestamp = separatedSentence[6];
          int ttff = Integer.parseInt(separatedSentence[7]);
          short numberOfSatellitesInView = Short.parseShort(separatedSentence[8]);
          double speedOverGround = Double.parseDouble(separatedSentence[9]);
          double courseOverGround = Double.parseDouble(separatedSentence[10]);
          psNMEA.setInt(1, startCounter);
          psNMEA.setDouble(2, localTimestamp);
          psNMEA.setDouble(3, longitude);
          psNMEA.setDouble(4, latitude);
          psNMEA.setDouble(5, altitude);
          psNMEA.setString(6, timestamp);
          psNMEA.setInt(7, ttff);
          psNMEA.setShort(8, numberOfSatellitesInView);
          psNMEA.setDouble(9, speedOverGround);
          psNMEA.setDouble(10, courseOverGround);
          psNMEA.addBatch();
          // otherwise the sentence is a processed CAN-sentence (contains 5
          // values)
        } else if (separatedSentence.length == 4) {
          
          String vehicleDataType = separatedSentence[2];
          String vehicleData = separatedSentence[3];
          // depending on the data type (e. g. RPM, fuel consumption etc.) we
          // process the sentence accordingly
          switch (vehicleDataType) {
          case "R":
            short rpm = (short) Double.parseDouble(vehicleData);
            psRPM.setInt(1, startCounter);
            psRPM.setDouble(2, localTimestamp);
            psRPM.setShort(3, rpm);
            psRPM.addBatch();
            break;
          case "F":
            double fuelConsumption = Double.parseDouble(vehicleData);
            psFuelConsumption.setInt(1, startCounter);
            psFuelConsumption.setDouble(2, localTimestamp);
            psFuelConsumption.setDouble(3, fuelConsumption);
            psFuelConsumption.addBatch();
            break;
          }
        }
        // safe batch within batch to limit inserts according to batch size
        if (++batchCount > batchSize) {
          if (psNMEA != null) {
            int[] results = psNMEA.executeBatch();
            insertNMEACounter = insertNMEACounter + results.length;
          }
          if (psRPM != null) {
            int[] results = psRPM.executeBatch();
            insertRPMCounter = insertRPMCounter + results.length;
          }
          if (psFuelConsumption != null) {
            int[] results = psFuelConsumption.executeBatch();
            insertFuelConsumptionCounter = insertFuelConsumptionCounter + results.length;
          }
          // reset batchCount
          batchCount = 0;
        }
      }
      // execute each batch
      if (psNMEA != null) {
        int[] results = psNMEA.executeBatch();
        insertNMEACounter = results.length;
      }
      if (psRPM != null) {
        int[] results = psRPM.executeBatch();
        insertRPMCounter = results.length;
      }
      if (psFuelConsumption != null) {
        int[] results = psFuelConsumption.executeBatch();
        insertFuelConsumptionCounter = results.length;
      }
    } catch (SQLException e) {
      e.printStackTrace();
      e.getNextException().printStackTrace();
    }
    int[] insertedRowsPerColumn = { insertNMEACounter, insertRPMCounter, insertFuelConsumptionCounter };
    return insertedRowsPerColumn;
  }

  /**
   * Returns a boolean which indicates the existence of a view.
   * 
   * @param viewName
   *          The name of the view as a string
   * @return a boolean stating the desired view does exist
   * 
   */
  public boolean viewExists(String viewName) {
    DatabaseMetaData meta = null;
    try (Connection conn = dataSource.getConnection()) {
      meta = conn.getMetaData();
    } catch (SQLException e) {
      e.printStackTrace();
    }
    try (ResultSet res = meta.getTables(null, null, viewName, new String[] { "VIEW" })) {
      try {
        if (res.next()) {
          // Table exists
          return true;
        } else {
          // Table does not exist
          return false;
        }
      } catch (SQLException e1) {
        e1.printStackTrace();
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }

  /**
   * Returns a boolean which indicates the existence of a table.
   * 
   * @param tableName
   *          The name of the table as a string
   * @return a boolean stating the desired table does exist
   * 
   */
  public boolean tableExists(String tableName) {
    DatabaseMetaData meta = null;
    try (Connection conn = dataSource.getConnection()) {
      meta = conn.getMetaData();
    } catch (SQLException e) {
      e.printStackTrace();
    }
    try (ResultSet res = meta.getTables(null, null, tableName, new String[] { "TABLE" })) {
      try {
        if (res.next()) {
          // Table exists
          return true;
        } else {
          // Table does not exist
          return false;
        }
      } catch (SQLException e1) {
        e1.printStackTrace();
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return false;
  }

  /**
   * Returns a Statement instance for use in a try-resources-section.
   * 
   * @param conn
   *          A Connection instance to a data source.
   * @param resultSetType
   *          A int constant to set the type of the result set
   * @param resultSetConcurrency
   *          A int constant to set the concurrency mode of the result set
   * @return A Statement instance
   * 
   */
  private Statement createStatement(Connection conn, int resultSetType, int resultSetConcurrency) throws SQLException {
    Statement stmt = conn.createStatement(resultSetType, resultSetConcurrency);
    return stmt;
  }

  /**
   * Returns a PreparedStatement instance for use in a try-resources-section.
   * 
   * @param conn
   *          A Connection instance to a data source.
   * @param sql
   *          A String which containts the parameterized SQL Statement
   * 
   */
  private PreparedStatement createPreparedStatement(Connection conn, String sql) throws SQLException {
    PreparedStatement ps = conn.prepareStatement(sql);
    return ps;
  }
}