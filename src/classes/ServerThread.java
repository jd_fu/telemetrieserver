package classes;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketTimeoutException;

/**
 * * A class that handles incoming data of the data logger using threads.
 * 
 * @author Dezhi Fu
 */
public class ServerThread implements Runnable {
  Server server;
  Socket socket;

  /**
   * Constructor which starts the thread and accepts a Server instance as well
   * as a Socket instance. The first object is used for reading the data the
   * last object is used for terminating the connection the second for reading
   * the data.
   */

  public ServerThread(Server server, Socket socket) {
    this.server = server;
    this.socket = socket;
    // start thread
    Thread thread = new Thread(this, "New server thread");
    thread.start();
    System.out.println(thread.getName());
  }

  /**
   * Process the data and remove the connection, when no further incoming data.
   */
  @Override
  public void run() {
    try {
      // process incoming data stream.
      processData(new DataInputStream(socket.getInputStream()));
    } catch (EOFException ie) {
      ie.printStackTrace();
    } catch (IOException ie) {
      ie.printStackTrace();
    } finally {
      // Connection will be closed
      server.removeConnection(socket);
    }
  }

  /**
   * Incoming data from socket is processed and delivered to SOS using this
   * method. Each incoming data sentence, i.e. semicolon separated values is
   * passed to DataSourceConnector instance to save the values in a postgresql
   * database.
   * 
   * @param din
   *          the input datastream as a DataInputStream instance, which is used
   *          to read the incoming data.
   */

  private void processData(DataInputStream din) {
    // Prepare database connection and database structure
    DataSourceConnector dbc = new DataSourceConnector(DataSourceConnectorFactory.getPostgresqlDataSource());
    dbc.createTables();
    dbc.createView();
    // Read data from socket input stream
    try {
      while (din.read() >= 0) {
        // allocate space for data input
        byte[] vehicleDataString = new byte[2048];
        // read the data
        try {
          din.read(vehicleDataString);
          // if timeout reached
        } catch (SocketTimeoutException e) {
          System.out.println("No incoming data from client. Closing socket...");
          break;
        } catch (IOException ioe) {
          ioe.printStackTrace();
        }
        // convert input from telemetry system to String
        String data = new String(vehicleDataString);
        System.out.println(data);
        // insert values into telemetrySystemDatabase
        dbc.insertValues(data);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}