package classes;

/**
 * A class to save the position and process data of a data logger in a vehicle.
 * A corresponding instance represents the position with a timestamp and
 * corresponding process data (e.g. fuel consumption).
 * 
 * @author Dezhi Fu
 */
public class Vehicle {
  private double longitude;
  private double latitude;
  private String timestamp;
  private int numberOfSatellitesTracked;
  private int engineRPM;
  private double fuelConsumption;

  public Vehicle(double longitude, double latitude, String timestamp, int numberOfSatellitesInView, int rpmValue,
      double fuelConsumptionValue) {
    setLongitude(convert2DecimalDegree(longitude));
    setLatitude(convert2DecimalDegree(latitude));
    setTimestamp(convertTimeStamp(timestamp));
    setNumberOfSatellitesViewed(numberOfSatellitesInView);
    setEngineRPM(rpmValue);
    setFuelConsumption(fuelConsumptionValue);
  }

  public Vehicle() {
  }

  /**
   * Performs the conversion of a coordinate from degree decimal minutes to
   * decimal degrees, i.e. from DDMM.mmm notation to DD.dddddd notation.
   * 
   * @param coordinate
   *          the input coordinate as a String, which is in degree decimal
   *          minutes, to convert to.
   * @return the coordinate as a double type in decimal degrees notation
   */
  private double convert2DecimalDegree(Double coordinate) {
    double deg = 0;
    double min = 0;
    deg = coordinate;
    // the two digits before and all digits after the dot correspond to decimal
    // minutes
    min += deg % 100;
    // the other digits before the dot correspond to our degrees
    deg = deg / 100;
    // convert minutes to decimal degrees and add that to the whole degrees
    deg = (int) deg + min / 60;
    return deg;
  }

  /**
   * Performs the conversion of a timestamp of the data logger to date time
   * expression as specified in ISO 8601
   * 
   * @param dateTimeString
   *          the input timestamp as a String, which is in the data logger's
   *          format, to convert to ISO 8601 notation.
   * @return the timestamp as a String, which is in ISO 8601 notation,
   *         containing separation characters and time zone designator
   */

  private String convertTimeStamp(String dateTimeString) {
    String date = dateTimeString.substring(0, 8);
    String timeStamp = dateTimeString.substring(8);
    date = date.substring(0, 4) + "-" + insertSymbol(date.substring(4), "-");
    String subTime = timeStamp.split("\\.")[1];
    String time = insertSymbol(timeStamp.split("\\.")[0], ":");
    return date + "T" + time + "." + subTime + "+00:00";
  }

  /**
   * Method to insert a user-defined character in a String
   * 
   * @param data
   *          the String in which a character has to be modified
   * @param symbol
   *          the character which we want to insert into the String
   * @return the new String with characters inserted
   */
  private String insertSymbol(String data, String symbol) {
    return data.replaceAll("..(?!$)", "$0" + symbol);
  }

  public boolean hasSameData(Vehicle vehicle) {
    return this.longitude == vehicle.getLongitude() && this.latitude == vehicle.getLatitude()
        && this.timestamp.equals(vehicle.getTimestamp())
        && this.numberOfSatellitesTracked == vehicle.getNumberOfSatellitesTracked()
        && this.engineRPM == vehicle.getEngineRPM() && this.fuelConsumption == vehicle.getFuelConsumption();
  }

  // Getters and Setters
  public double getLatitude() {
    return latitude;
  }

  public void setLatitude(double latitude) {
    this.latitude = latitude;
  }

  public double getLongitude() {
    return longitude;
  }

  public void setLongitude(double longitude) {
    this.longitude = longitude;
  }

  public String getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

  public int getNumberOfSatellitesTracked() {
    return numberOfSatellitesTracked;
  }

  public void setNumberOfSatellitesViewed(int numberOfSatellitesTracked) {
    this.numberOfSatellitesTracked = numberOfSatellitesTracked;
  }

  public double getFuelConsumption() {
    return fuelConsumption;
  }

  public void setFuelConsumption(double fuelConsumption) {
    this.fuelConsumption = fuelConsumption;
  }

  public int getEngineRPM() {
    return engineRPM;
  }

  public void setEngineRPM(int engineRPM) {
    this.engineRPM = engineRPM;
  }
}
