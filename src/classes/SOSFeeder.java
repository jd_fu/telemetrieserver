package classes;

import java.util.List;
import org.jdom2.Document;

/**
 * A class that handles fetching data from database view to submit to SOS
 * instance using threads.
 * 
 * @author Dezhi Fu
 */
public class SOSFeeder implements Runnable {

  /**
   * Constructor which starts the thread.
   * 
   */
  public SOSFeeder() {
    Thread thread = new Thread(this, "New SOSFeeder thread");
//    thread.start();
    System.out.println(thread.getName() + " started");
  }

  /**
   * Fetch the data and submit to SOS instance.
   * 
   */

  @Override
  public void run() {
    submitGeoreferencedTelemetryData();
  }

  /**
   * Telemetry data are fetched using the getVehicleData()-method to further
   * pass them to XML-documents, which are encoded correspondingly to
   * O&M-Format. Finally those documents are submitted to SOS using HTTP-POST.
   * 
   */

  public void submitGeoreferencedTelemetryData() {
    // Prepare the SOSClient instance in order to execute HTTP POST
    SOSClient sos = new SOSClient();
    // Prepare database connection
    DataSourceConnector dbc = new DataSourceConnector(DataSourceConnectorFactory.getPostgresqlDataSource());
    dbc.createTables();
    dbc.createView();
    while (true) {
      // fetch vehicle data
      List<Vehicle> vehicleData = dbc.getVehicleData();
      System.out.println(vehicleData.size());
      // if there is any data
      if (vehicleData.size() != 0) {
        for (Vehicle v : vehicleData) {
          // parse to O&M
          XMLParser parser = new XMLParser();
          Document observationsAndMeasurementsDocument = parser.readXML("/resources/InsertObservation.xml").marshal(v);
          // submit to SOS instance
          sos.InsertObservation(observationsAndMeasurementsDocument);
        }
      }
    }
  }
}
