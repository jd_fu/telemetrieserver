package main;

import classes.SOSFeeder;
import classes.Server;

/**
 * Entry point of data logger program on server side. We wait for incoming
 * socket-based connection attempt on port 2345. Furthermore we initiate the
 * SOSFeeder.
 * 
 * @author Dezhi Fu
 */
public class Main {
  public static void main(String[] args) {
    // Initiate thread which deals with submitting georeferenced telemetry data
    // to SOS instance
    new SOSFeeder();
    // start Server instance
    new Server(2345);
  }
}