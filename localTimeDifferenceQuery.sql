﻿
SELECT DISTINCT ON(nmeadata.id)
  nmeadata.id as nmeadata_id,
  rpmdata.id as rpmdata_id,
  fuelconsumptiondata.id as fuelconsumption_id,
  nmeadata.start_counter as nmeadata_start_counter,
  rpmdata.start_counter as rpmdata_start_counter,
  fuelconsumptiondata.start_counter as fuelconsumptiondata_start_counter,
  abs(nmeadata.local_time-rpmdata.local_time_identity) as abs_time_difference_rpmdata,
  abs(nmeadata.local_time-fuelconsumptiondata.local_time_identity) as abs_time_difference_fuelconsumptiondata
FROM 
  public.nmeadata, 
  public.rpmdata, 
  public.fuelconsumptiondata
  WHERE
  nmeadata.start_counter = rpmdata.start_counter AND rpmdata .start_counter = fuelconsumptiondata.start_counter
ORDER BY
  nmeadata_id, abs_time_difference_rpmdata, abs_time_difference_fuelconsumptiondata ASC
