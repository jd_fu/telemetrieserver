﻿SELECT
  nmeadata.id,
  nmeadata.longitude,
  nmeadata.latitude,
  nmeadata.altitude,
  nmeadata.utc,
  nmeadata.time_to_first_fix,
  nmeadata.number_of_satellites_in_view,
  nmeadata.speed_over_ground,
  nmeadata.course_over_ground,
  rpmdata.rpm_value, 
  fuelconsumptiondata.fuel_consumption_value 
FROM
 nmeadata, rpmdata, fuelconsumptiondata, (
SELECT DISTINCT ON(nmeadata.id)
  nmeadata.id as nmeadata_id,
  rpmdata.id as rpmdata_id,
  fuelconsumptiondata.id as fuelconsumption_id,
  nmeadata.start_counter as nmeadata_start_counter,
  rpmdata.start_counter as rpmdata_start_counter,
  fuelconsumptiondata.start_counter as fuelconsumptiondata_start_counter,
  abs(nmeadata.local_time-rpmdata.local_time_identity) as abs_time_difference_rpmdata,
  abs(nmeadata.local_time-fuelconsumptiondata.local_time_identity) as abs_time_difference_fuelconsumptiondata
FROM 
  public.nmeadata, 
  public.rpmdata, 
  public.fuelconsumptiondata
WHERE
  nmeadata.start_counter = rpmdata.start_counter AND rpmdata .start_counter = fuelconsumptiondata.start_counter
ORDER BY
  nmeadata_id, abs_time_difference_rpmdata, abs_time_difference_fuelconsumptiondata ASC
  )inner_table
WHERE (nmeadata.id = nmeadata_id AND rpmdata.id = inner_table.rpmdata_id AND fuelconsumptiondata.id = inner_table.fuelconsumption_id) AND (abs_time_difference_rpmdata < 1 AND abs_time_difference_fuelconsumptiondata < 1)