﻿SELECT
  nmeadata.longitude,
  nmeadata.latitude,
  nmeadata.altitude,
  nmeadata.utc,
  nmeadata.time_to_first_fix,
  nmeadata.number_of_satellites_in_view,
  nmeadata.speed_over_ground,
  nmeadata.course_over_ground,
  rpmdata.rpm_value, 
  fuelconsumptiondata.fuel_consumption_value 
FROM
 nmeadata, rpmdata, fuelconsumptiondata, (
SELECT DISTINCT ON (nmeadata_id)
  nmeadata.id as nmeadata_id,
  rpmdata.id as rpmdata_id,
  fuelconsumptiondata.id as fuelconsumption_id,
  abs(nmeadata.local_time-rpmdata.local_time_identity) as abs_time_difference_rpmdata,
  abs(nmeadata.local_time-fuelconsumptiondata.local_time_identity) as abs_time_difference_fuelconsumptiondata
FROM 
  public.nmeadata, 
  public.rpmdata, 
  public.fuelconsumptiondata
ORDER BY
  nmeadata_id, abs_time_difference_rpmdata, abs_time_difference_fuelconsumptiondata ASC
  )inner_table
WHERE (nmeadata.id = nmeadata_id AND rpmdata.id = inner_table.rpmdata_id AND fuelconsumptiondata.id = inner_table.fuelconsumption_id) AND (abs_time_difference_rpmdata < 0.5 AND abs_time_difference_fuelconsumptiondata < 0.5)