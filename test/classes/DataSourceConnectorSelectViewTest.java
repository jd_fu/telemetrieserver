package classes;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * A JUnit test to check the check the select view functionality of
 * DataSourceConnector class.
 * 
 */
public class DataSourceConnectorSelectViewTest {
  DataSourceConnector dbc = null;
  Vehicle expectedVehicleData = new Vehicle();
  Vehicle actualVehicleData = new Vehicle();
  // testdata Strings
  private static String nmeaData = "6_1469695189.109694,0,1134.076920,4808.975868,614.856750,20160728210207.000,66,9,0.000000,0.000000;"
      + "6_1469695190.109694,0,1134.090132,4808.985123,619.856750,20160728210209.000,66,10,0.000000,0.000000;"
      + "6_1469695190.209694,0,1134.090132,4808.985123,619.856750,20160728210310.000,66,10,0.000000,0.000000;";
  private static String rpmData = "6_1469695189.2975189,R,1112;" + "6_1469695189.3075189,R,1002;"
      + "6_1469695190.1075189,R,1302;" + "6_1469695190.2075189,R,1402;";
  private static String fuelData = "6_1469695189.2985189,F,3.1;"
      + "6_1469695189.3285189,F,3.1;" + "6_1469695190.3985189,F,5.1;"
      + "6_1469695190.4985189,F,7.1;";
  private static String inputData = nmeaData + rpmData + fuelData;

  @Before
  public void setup() {
    dbc = new DataSourceConnector(DataSourceConnectorFactory.getTestPostgresqlDataSource());
    dbc.dropTables();
    dbc.createTables();
    dbc.insertValues(inputData);
    dbc.createView();
    expectedVehicleData.setLongitude(11.567948666666666);
    expectedVehicleData.setLatitude(48.14959780000001);
    expectedVehicleData.setTimestamp("2016-07-28T21:02:07.000+00:00");
    expectedVehicleData.setNumberOfSatellitesViewed(9);
    expectedVehicleData.setEngineRPM(1112);
    expectedVehicleData.setFuelConsumption(3.1);
    for (Vehicle v : dbc.getVehicleData()) {
      actualVehicleData = v;
      System.out.println(actualVehicleData);
      break;
    }
  }

  @Test
  public void testSelectView() {
    assertTrue("Vehicle data are not the same", expectedVehicleData.hasSameData(actualVehicleData));
  }

  @After
  public void teardown() {
    dbc.dropTables();
    dbc.closeDataSource();
  }
}