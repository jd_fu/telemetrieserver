package classes;

import java.io.IOException;

import org.custommonkey.xmlunit.XMLTestCase;
import org.custommonkey.xmlunit.XMLUnit;
import org.jdom2.Document;
import org.jdom2.output.XMLOutputter;
import org.junit.Test;
import org.xml.sax.SAXException;

import classes.XMLParser;

/**
 * A JUnit test to check the marshalling process.
 * 
 */
public class MarshalTest extends XMLTestCase {

  @Test
  public void testMarshalSensorValues() throws SAXException, IOException {
    Vehicle sampleVehicleData = new Vehicle();

    sampleVehicleData.setLongitude(11.607488049999999);
    sampleVehicleData.setLatitude(48.20297776666666);
    sampleVehicleData.setTimestamp("2016-01-30T14:56:47.000+00:00");
    sampleVehicleData.setNumberOfSatellitesViewed(13);
    sampleVehicleData.setEngineRPM(1232);
    sampleVehicleData.setFuelConsumption(3.3);
    String toBeComparedOnOAndMDocument = "";

    XMLParser parser = new XMLParser();
    toBeComparedOnOAndMDocument = new XMLOutputter()
        .outputString(parser.readXML("/resources/InsertObservationTestPassed.xml").getXmlDocument());
    XMLUnit.setIgnoreWhitespace(true);

    Document observationsAndMeasurementsDocument = parser.readXML("/resources/InsertObservation.xml")
        .marshal(sampleVehicleData);
    String toBeTestedOAndMDocument = new XMLOutputter().outputString(observationsAndMeasurementsDocument);
    assertXMLEqual(toBeComparedOnOAndMDocument, toBeTestedOAndMDocument);
  }
}
