package classes;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * A JUnit test to check the check the insert table functionality of
 * DataSourceConnector class.
 * 
 */
public class DataSourceConnectorInsertTableTest {
  DataSourceConnector dbc = null;

  // testdata Strings
  private static String nmeaData = "6_1469695189.109694,0,1134.076920,4808.975868,614.856750,20160728210207.000,66,9,0.000000,0.000000;"
      + "6_1469695190.109694,0,1134.090132,4808.985123,619.856750,20160728210209.000,66,10,0.000000,0.000000;"
      + "6_1469695190.209694,0,1134.090132,4808.985123,619.856750,20160728210310.000,66,10,0.000000,0.000000;";
  private static String rpmData = "6_1469695189.2975189,R,1112;" + "6_1469695189.3075189,R,1002;"
      + "6_1469695190.1075189,R,1302;+" + "6_1469695190.2075189,R,1402;";
  private static String fuelData = "6_1469695189.2985189,F,3.1;"
      + "6_1469695189.3285189,F,3.1;" + "6_1469695190.3985189,F,5.1;"
      + "6_1469695190.4985189,F,7.1;";
  private static String inputData = nmeaData + rpmData + fuelData;

  @Before
  public void setup() {
    dbc = new DataSourceConnector(DataSourceConnectorFactory.getTestPostgresqlDataSource());
    dbc.dropTables();
    dbc.createTables();
  }

  @Test
  public void testInsertTables() {
    final int[] expectedNumberOfRowsInserted = { 3, 4, 4 };
    int[] actualNumberOfRowsInserted = dbc.insertValues(inputData);
    Assert.assertArrayEquals(expectedNumberOfRowsInserted, actualNumberOfRowsInserted);

  }

  @After
  public void teardown() {
    dbc.dropTables();
    dbc.closeDataSource();
  }
}
