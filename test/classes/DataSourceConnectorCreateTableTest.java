package classes;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * A JUnit test to check the check the create table functionality of
 * DataSourceConnector class.
 * 
 */
public class DataSourceConnectorCreateTableTest {
  DataSourceConnector dbc = null;

  @Before
  public void setup() {
    dbc = new DataSourceConnector(DataSourceConnectorFactory.getTestPostgresqlDataSource());
    dbc.dropTables();
  }

  @Test
  public void testCreateTables() {
    DataSourceConnector dbc = new DataSourceConnector(DataSourceConnectorFactory.getTestPostgresqlDataSource());
    dbc.createTables();
    assertTrue(dbc.tableExists("nmeadata"));
    assertTrue(dbc.tableExists("rpmdata"));
    assertTrue(dbc.tableExists("fuelconsumptiondata"));
  }

  @After
  public void teardown() {
    dbc.dropTables();
    dbc.closeDataSource();
  }
}