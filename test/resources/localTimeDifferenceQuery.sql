﻿SELECT 
  nmeadata.id as nmeadata_id,
  rpmdata.id as rpmdata_id,
  fuelconsumptiondata.id as fuelconsumption_id,
  abs(nmeadata.local_time-rpmdata.local_time_identity) as abs_time_difference_rpmdata,
  abs(nmeadata.local_time-fuelconsumptiondata.local_time_identity) as abs_time_difference_fuelconsumptiondata
FROM 
  public.nmeadata, 
  public.rpmdata, 
  public.fuelconsumptiondata
ORDER BY
  nmeadata_id, abs_time_difference_rpmdata, abs_time_difference_fuelconsumptiondata ASC
